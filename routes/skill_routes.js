var express = require('express');
var router = express.Router();
var skill_dal = require('../model/skill_dal');



// View All Addresses
router.get('/all', function(req, res) {
    skill_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('skill/skillViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.skillID == null) {
        res.send('skillID is null');
    }
    else {
        skill_dal.getById(req.query.skillID, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('skill/skillViewByID', {'result': result});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    skill_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('skill/skillAdd');
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.skillName == null)
    {
        res.send('A skill name must be provided ');
    }
    else if(req.query.description == null)
    {
        res.send('A description must be provided');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        skill_dal.insert(req.query, function(err,result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/skill/all');
            }
        });
    }
});

router.get('/edit', function(req, res)
{
    if(req.query.skillID == null)
    {
        res.send('A skill ID is required');
    }
    else
    {
        skill_dal.edit(req.query.skillID, function(err, result)
        {
            res.render('skill/skillUpdate', {skill: result[0][0]});
        });
    }

});

router.get('/update', function(req, res)
{
    skill_dal.update(req.query, function(err, result)
    {
        res.redirect(302, '/skill/all');
    });
});

// Delete a company for the given companyID
router.get('/delete', function(req, res){
    if(req.query.skillID == null) {
        res.send('skillID is null');
    }
    else {
        skill_dal.delete(req.query.skillID, function(err, result)
        {
            if(err)
            {
                res.send(err);
            }
            else
            {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/skill/all');
            }
        });
    }
});

module.exports = router;
