var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');


// View All resume
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});

// View the resume for the given id
router.get('/', function(req, res){
    if(req.query.resumeID === null) {
        res.send('resumeID is null');
    } else {
        resume_dal.getById(req.query.resumeID, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('resume/resumeViewById', {'result': result});
            }
        });
    }
});

// Return the add a new resume form
router.get('/add', function(req, res)
{

    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.getAllResumeInfo(req.query, function(err,result)
    {
        if (err)
        {
            res.send(err);
        }
        else {
            console.log(result);
            res.render('resume/resumeAdd',
                {
                'accountInfo': result[0][0],
                'company': result[1],
                'school': result[2],
                'skill': result[3]
            });
        }
    });
});



router.get('/resumeSelectUser', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.getAllUsers(function(err,result)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
            res.render('resume/resumeSelectUser', {'result': result});
        }
    });
});

// View the resume for the given id
router.get('/insert', function(req, res)
{
    // simple validation
    if(req.query.resumeName === null)
    {
        res.send('Resume Name must be provided.');
    }
    else if(req.query.schoolID == null)
    {
        res.send('schoolID must be provided');
    }
    else if(req.query.companyID == null)
    {
        res.send('companyID must be provided');
    }
    else if(req.query.skillID == null)
    {
        res.send('skillID must be provided');
    }
    else
    {
        resume_dal.insert(req.query, function(err, result)
        {
            resume_dal.insertSchool(result.insertId, req.query.schoolID, function(err, school)
            {
                resume_dal.insertCompany(result.insertId, req.query.companyID, function(err, company)
                {
                    resume_dal.insertSkill(restult.insertId, req.query.skillID, function(err,skill)
                    {

                    });
                });

            });
            if(err)
            {
              res.send(err);
            }
            else
            {
                res.send('success');
            }

        });
    };

});


// Delete an resume for the given resume_id
router.get('/delete', function(req, res)
{
    if(req.query.resumeID === null) {
        res.send('resumeID is null');
    }
    else {
        resume_dal.delete(req.query.resumeID, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/resume/all');
            }
        });
    }
});

module.exports = router;
