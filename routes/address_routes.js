var express = require('express');
var router = express.Router();
var address_dal = require('../model/address_dal');


// View All Addresses
router.get('/all', function(req, res)
{
    address_dal.getAll(function(err, result)
    {
        if(err)
        {
            res.send(err);
        }
        else
        {
            res.render('address/addressViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.addressID == null)
    {
        res.send('addressID is null');
    }
    else
    {
        address_dal.getById(req.query.addressID, function(err,result)
        {
            if (err)
            {
                res.send(err);
            }
            else
            {
                res.render('address/addressViewByID', {'result': result});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    address_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('address/addressAdd');
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res)
{
    // simple validation
    if(req.query.street == null)
    {
        res.send('A street must be provided ');
    }
    else if(req.query.zipCode == null)
    {
        res.send('A zip code must be provided');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        address_dal.insert(req.query, function(err,result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
                {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/address/all');
            }
        });
    }
});

router.get('/edit', function(req, res)
{
    if(req.query.addressID == null)
    {
        res.send('An address ID is required');
    }
    else
    {
        address_dal.edit(req.query.addressID, function(err, result)
        {
            res.render('address/addressUpdate', {address: result[0][0]});
        });
    }

});

router.get('/update', function(req, res) {
    address_dal.update(req.query, function(err, result)
    {
        res.redirect(302, '/address/all');
    });
});

// Delete a company for the given companyID
router.get('/delete', function(req, res){
    if(req.query.addressID == null) {
        res.send('addressID is null');
    }
    else {
        address_dal.delete(req.query.addressID, function(err, result)
        {
            if(err)
            {
                res.send(err);
            }
            else
            {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/address/all');
            }
        });
    }
});

module.exports = router;
