var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result)
    {
        callback(err, result);
    });
};

exports.getById = function(accountID, callback)
{
    var query = 'SELECT * FROM account WHERE accountID = ?;';
    var queryData = [accountID];
    console.log(query);

    connection.query(query, queryData, function(err, result)
    {

        callback(err, result);
    });
};

exports.insert = function(params, callback)
{

    // FIRST INSERT THE account
    var query = 'INSERT INTO account (firstName, lastName, email) VALUES (?, ?, ?)';

    var queryData = [params.firstName, params.lastName, params.email];

    connection.query(query, queryData, function(err, result)
    {
            callback(err, result);
    });

};

exports.update = function(params, callback)
{
    var query = 'UPDATE account SET firstName = ?, lastName = ?, email = ? WHERE accountID = ?';
    var queryData = [params.email, params.firstName, params.lastName, params.accountID];

    connection.query(query, queryData, function (err, result)
    {
        callback(err, result);
    });
};

exports.delete = function(accountID, callback)
{
    var query = 'DELETE FROM account WHERE accountID = ?;';
    var queryData = [accountID];

    connection.query(query, queryData, function (err, result)
    {
        callback(err, result);
    });
};


/*
STORED PROCEDURE USED IN THIS FUNCTION

 DROP PROCEDURE IF EXISTS accountGetInfo;

 DELIMITER //
 CREATE PROCEDURE accountGetInfo(_accountID INT)

 BEGIN

 SELECT * FROM account WHERE accountID = _accountID;

 END //
 DELIMITER ;
 */


exports.edit = function(accountID, callback)
{
    var query = 'CALL accountGetInfo(?);';
    var queryData = [accountID];

    connection.query(query, queryData, function (err, result)
    {
        callback(err, result);
    });
};
