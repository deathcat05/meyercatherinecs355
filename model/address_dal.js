var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result)
    {
        callback(err, result);
    });
};

/* VIEW USED:

CREATE OR REPLACE VIEW addressView AS
SELECT a.* from address a;
 */

exports.getById = function(addressID, callback)
{
  var query = 'SELECT * FROM address WHERE addressID = ?;';
  var queryData = [addressID];
  console.log(query);

    connection.query(query, queryData, function(err, result)
    {
        callback(err, result);
    });
};

exports.insert = function(params, callback)
{
  var query = 'INSERT INTO address (street, zipCode) VALUES (?);';
  var queryData = [params.street, params.zipCode];

    connection.query(query, queryData,  function(err, result)
    {
        callback(err, result);
    });
};

exports.update = function(params, callback)
{
  var query = 'UPDATE address SET street = ? , zipCode = ? WHERE addressID = ?;';
  var queryData =[params.street, params.zipCode, params.addressID];

    connection.query(query, queryData, function(err, result)
    {
        callback(err, result);
    });
};

exports.delete = function(addressID, callback)
{
  var query = 'DELETE FROM address WHERE addressID = ?;';
  var queryData = [addressID];

    connection.query(query, queryData, function(err, result)
    {
        callback(err, result);
    });
};

/*
STORED PROCEDURE USED HERE:

 DROP PROCEDURE IF EXISTS addressGetInfo;

 DELIMITER //
 CREATE PROCEDURE addressGetInfo (_addressID INT)
 BEGIN

 SELECT * FROM address WHERE _addressID = addressID;

 END //
 DElIMITER ;

 */

exports.edit = function(addressID, callback)
{
  var query = 'CALL addressGetInfo(?);';
  var queryData = [addressID];

    connection.query(query, queryData, function(err, result)
    {
        callback(err, result);
    });
};
