var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback)
{
    var query = 'SELECT * FROM skill;';

    connection.query(query, function(err, result)
    {
        callback(err, result);
    });
};

/* VIEW USED:

 CREATE OR REPLACE VIEW skillView AS
 SELECT s.* from skill s;
 */

exports.getById = function(skillID, callback)
{
    var query = 'SELECT * FROM skill WHERE skillID = ?;';
    var queryData = [skillID];
    console.log(query);

    connection.query(query, queryData, function(err, result)
    {
        callback(err, result);
    });
};

exports.insert = function(params, callback)
{
    var query = 'INSERT INTO skill (skillName, description) VALUES (?, ?);';
    var queryData = [params.skillName, params.description, params.skillID];

    connection.query(query, queryData, function(err, result)
    {
        callback(err, result);
    });
};

exports.update = function(params, callback)
{
    var query = 'UPDATE skill SET skillName = ? , description = ? WHERE skillID = ?;';
    var queryData =[params.skillName, params.description, params.skillID];

    connection.query(query, queryData, function(err, result)
    {
        callback(err, result);
    });
};

exports.delete = function(skillID, callback)
{
    var query = 'DELETE FROM skill WHERE skillID = ?;';
    var queryData = [skillID];

    connection.query(query, queryData, function(err, result)
    {
        callback(err, result);
    });
};

/*
 STORED PROCEDURE USED HERE:

 DROP PROCEDURE IF EXISTS skillGetInfo;

 DELIMITER //
 CREATE PROCEDURE skillGetInfo (_skillID INT)
 BEGIN

 SELECT * FROM skill WHERE _skillID = skillID;

 END //
 DElIMITER ;

 */

exports.edit = function(skillID, callback)
{
    var query = 'CALL skillGetInfo(?);';
    var queryData = [skillID];

    connection.query(query, queryData, function(err, result)
    {
        callback(err, result);
    });
};
