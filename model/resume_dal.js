var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view account_view as
 select a.* from account a;
 */


exports.getAll = function(callback) {
    var query = 'SELECT r.*, a.accountID, a.firstName, a.lastName ' +
        'FROM resume r LEFT JOIN account a on r.accountID = a.accountID ' +
        'ORDER BY a.firstName';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};



exports.getAllResumeInfo = function(params, callback) {
    var query = 'CALL accountResumeGetInfo(?)';
    var queryData = [params.accountID];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.getAllUsers = function(callback)
{
    var query = 'SELECT * FROM account' ;

    connection.query(query, function(err, result)
    {
        callback(err, result);
    });
};


exports.getById = function(accountID, callback) {
    var query = 'SELECT r.*, a.firstName, a.lastName ' +
        'FROM resume r LEFT JOIN account a on r.accountID = a.accountID ' +
        'WHERE r.resumeID = ?;';
    var queryData = [accountID];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(accountID, resumeName, callback) {

    var query = 'INSERT INTO resume (accountID, resumeName) VALUES (?,?)';
    var queryData = [accountID, resumeName];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resumeName = ? ';
    var queryData = [params.resumeName, params.accountID];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};


exports.delete = function(resumeID, callback) {
    var query = 'DELETE FROM resume WHERE resumeID = (?)';
    var queryData = [resumeID];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.insertSchool = function (resumeID, schoolIdArray, callback) {
    var query = 'INSERT INTO  resumeSchool (resumeID, schoolID) VALUES ?';


    var resumeSchoolData = [];
    if (schoolIdArray.constructor === Array) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            resumeSchoolData.push([resumeID, schoolIdArray[i]]);
        }
    }
    else {
        resumeSchoolData.push([resumeID, schoolIdArray]);
    }

    connection.query(query, [resumeSchoolData], function(err, result) {
        callback(err, result);
    });
};


exports.insertSkill = function (resumeID, skillIdArray, callback) {
    var query = 'INSERT INTO  resumeSkill (resumeID, skillID) VALUES ?';
    var resumeSkillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            resumeSkillData.push([resumeID, skillIdArray[i]]);
        }
    }
    else {
        resumeSkillData.push([resumeID, skillIdArray]);
    }

    connection.query(query, [resumeSkillData], function(err, result) {
        callback(err, result);
    });
};



exports.insertCompany = function (resumeID, companyIdArray, callback) {
    var query = 'INSERT INTO  resumeCompany (resumeID, companyID) VALUES ?';
    var resumeCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
            resumeCompanyData.push([resumeID, companyIdArray[i]]);
        }
    }
    else {
        resumeCompanyData.push([resumeID, companyIdArray]);
    }

    connection.query(query, [resumeCompanyData], function(err, result) {
        callback(err, result);
    });

};



/*
 DROP PROCEDURE IF EXISTS account_resume_getinfo;

 DELIMITER //
 CREATE PROCEDURE account_resume_getinfo (_account_id int)
 BEGIN

 SELECT * FROM account WHERE account_id = _account_id;

 SELECT c.*, ac.* FROM company c
 LEFT JOIN account_company ac ON c.company_id = ac.company_id AND account_id = _account_id;

 SELECT s.*, sa.* FROM school s
 LEFT JOIN account_school sa ON s. school_id = sa.school_id AND account_id = _account_id;

 SELECT sk.*, ska.* FROM skill sk
 LEFT JOIN account_skill ska ON sk.skill_id = ska.skill_id AND account_id = _account_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL account_getinfo (2);
 */

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS account_getinfo;

 DELIMITER //
 CREATE PROCEDURE account_getinfo(_account_id int)
 BEGIN
 SELECT * FROM account WHERE account_id = _account_id;

 END; //

 # Call the Stored Procedure
 CALL account_getinfo (4);

 */

exports.edit = function(accountID, callback) {
    var query = 'CALL accountGetInfo(?)';
    var queryData = [accountID];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


